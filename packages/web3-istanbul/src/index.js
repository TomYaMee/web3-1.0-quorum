/*
    This file is part of web3.js.

    web3.js is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    web3.js is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with web3.js.  If not, see <http://www.gnu.org/licenses/>.
*/
/** @file index.js
 * @author
 * @date 2018
 */

"use strict";
var core = require('../../web3-core/src/index.js');
var Method = require('../../web3-core-method/src/index.js');

var Istanbul = function(){
  core.packageInit(this, arguments);

  var _this = this;

  [
    new Method({
        name: 'getSnapshot',
        call: 'istanbul_getSnapshot',
        params: 1
    }),
    new Method({
        name: 'getSnapshotAtHash',
        call: 'istanbul_getSnapshotAtHash',
        params: 1
    }),
    new Method({
        name: 'getValidators',
        call: 'istanbul_getValidators',
        params: 1
    }),
    new Method({
        name: 'getValidatorsAtHash',
        call: 'istanbul_getValidatorsAtHash',
        params: 1
    }),
    new Method({
        name: 'propose',
        call: 'istanbul_propose',
        params: 2
    }),
    new Method({
        name: 'discard',
        call: 'istanbul_discard',
        params: 1
    }),
    new Method({
        name: 'candidates',
        call: 'istanbul_candidates',
        params: 0
    })
  ].forEach(function(method) {
      method.attachToObject(_this);
      method.setRequestManager(_this._requestManager);
  });
};

core.addProviders(Istanbul);

module.exports = Istanbul;
